package com.sapient;

public class Concat {

	public static void main(String[] args) {
	compareStrings(new String(), new String(), new String());
	}
	
	public static String compareStrings(String firstString, String secondString, String thirdString) {
        String one = null;
        String two = null;
        String three = null;

        if((firstString.compareTo(secondString)<=0) && (firstString.compareTo(thirdString)<=0)) {
            one = firstString;
            if(secondString.compareTo(thirdString) <=0) {
            	two = secondString;
            	three = thirdString;
            } else {
            	three = secondString;
            	two = thirdString;
            }
        } else if(secondString.compareTo(firstString) <=0 && secondString.compareTo(thirdString) <=0) {
        	one = secondString;
        	if(firstString.compareTo(thirdString) <=0) {
            	two = firstString;
            	three = thirdString;
            } else {
            	three = firstString;
            	two = thirdString;
            }
        } else if(thirdString.compareTo(firstString) <=0 && thirdString.compareTo(secondString)<=0) {
        	one = thirdString;
        	if(secondString.compareTo(firstString) <= 0) {
        		two = secondString;
        		three= firstString;
        	} else {
        		two = firstString;
        		three = secondString;
        	}
        }
        System.out.println(one + two + three);
        return null;

    }

}
