package com.myzee.array;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.Random;
import java.util.Stack;

public class SecondHighestElementInArray {

	public static void main(String[] args) {
//		int[] a = new int[]{1,2,3,4,5};
		
		int size = 8;
		int[] a = new int[size];
		
		Random rand = new Random();
		for(int i = 0; i < size; i++) {
			int num = rand.nextInt(100);
			a[i] = num;
		}
		
		int max = a[0];
		int secMax = a[0];
		System.out.print("{");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + ", ");
			if(a[i] > max) {
				secMax = max;
				max = a[i];
			} else if(a[i] > secMax) {
				secMax = a[i];
			}
		}
		System.out.print("}\n\n");
		System.out.println("Highest element is - " + max);
		System.out.println("Second highest is - " + secMax);
		/*
		Stack<Integer> stack = new Stack<>();
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
		
		Iterator<Integer>itr = stack.iterator();
		while (itr.hasNext()) {
			Integer i = (Integer) itr.next();
			System.out.println(i);
		}
		int i = 1;
		int stackSize = stack.size();
		while(i++ <= stackSize) {
			System.out.println(stack.pop());
		}
		*/
	}

}
