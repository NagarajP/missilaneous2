package com.myzee.linkedlist;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


class MyLinkedList2 {
	NewNode3 head = null;
	
	public void insert(NewNode3 node) {
		if(head == null) {
			head = node;
		} else {
			NewNode3 cur = head;
			while(cur.next != null) {
				cur = cur.next;
			}
			cur.next = node;
		}
	}
	
	public void display() {
		NewNode3 cur = head;
		System.out.println("\ndisplay list: ");
		while(cur != null) {
			System.out.print(cur.data + ", ");
			cur = cur.next;
		}
	}
	
}

class NewNode3 {
	int data;
	NewNode3 next;
	
	public NewNode3(int data) {
		this.data = data;
		this.next = null;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		NewNode3 n = (NewNode3) obj;
		if(this.data == n.data) {
			return true;
		}
		return false;
	}

}

public class FirstDuplicateInList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyLinkedList2 list = new MyLinkedList2();
		
		list.insert(new NewNode3(3));
		list.insert(new NewNode3(5));
		list.insert(new NewNode3(1));
		list.insert(new NewNode3(1));
		list.insert(new NewNode3(4));
		list.insert(new NewNode3(6));
		list.insert(new NewNode3(5));
		list.display();
		findDuplicate(list);
	}

	public static void findDuplicate(MyLinkedList2 list) {
		NewNode3 cur = list.head;
		List<NewNode3> alist = new ArrayList<NewNode3>();
		int count = 1;
		while(cur.next != null) {
			if(alist.contains(cur)) {
				System.out.println("\nduplicate '" +cur.data + "' found at "+ count++ + "th position");
				break;
			}
			count++;
			alist.add(cur);
			cur = cur.next;
		}
		if(cur.next == null) {
			System.out.println("\nNo duplicates found");
		}
	}

}

