package com.myzee.linkedlist;

public class CustomLinkedList {

	public static void main(String[] args) {
		MyLinkedList list = new MyLinkedList();
		int count = 1;
		while(count <= 5) {
			NewNode node = new NewNode(count++);
			list.insert(node);
		}
		list.display();
	}
}

class MyLinkedList {
	NewNode head = null;
	
	public void insert(NewNode node) {
		if(head == null) {
			head = node;
		} else {
			NewNode cur = head;
			while(cur.next != null) {
				cur = cur.next;
			}
			cur.next = node;
		}
	}
	
	public void display() {
		NewNode cur = head;
		System.out.println("\ndisplay list: ");
		while(cur != null) {
			System.out.print(cur.data + ", ");
			cur = cur.next;
		}
	}
	
}

class NewNode {
	int data;
	NewNode next;
	
	public NewNode(int data) {
		this.data = data;
		this.next = null;
	}

}
