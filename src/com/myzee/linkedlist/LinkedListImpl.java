package com.myzee.linkedlist;

import java.util.Scanner;

class Node {
	int data;
	Node next;

	public Node(int data) {
		this.data = data;
		this.next = null;
	}
}

class LinkedListBoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public LinkedListBoundException(String msg) {
		// TODO Auto-generated constructor stub
		super(msg);
	}
}

class LinkedList {
	static Node head = null;

	public static LinkedList addNode(LinkedList list, Node node) {
		if (list.head == null) {
			list.head = node;
		} else {
			Node temp = list.head;
			while (temp.next != null) {
				temp = temp.next;
			}
			temp.next = node;
		}
		return list;
	}

	public static LinkedList deleteHead(LinkedList list) {
		if (list.head == null) {
			System.out.println("Linked list is EMPTY!!");
		} else {
			System.out.println("Head deleted - " + list.head.data);
			list.head = list.head.next;
		}
		return list;
	}

	public static LinkedList deleteAtPosition(LinkedList list, int pos) {

		if (list.head == null || pos <= 0) {
			System.out.println("Error: List is empty or position does not exist");
			return list;
		}
		if (pos == 1) {
			return deleteHead(list);
		}

		Node curNode = list.head;
		Node prev = null;
		int count = 1;
		while(curNode != null) {
			if (pos == count) {
				Node deleted = curNode;
				prev.next = curNode.next;
				System.out.println("Node deleted at " + pos + " is: " + deleted.data);
				break;
			} else {
				prev = curNode;
				curNode = curNode.next;
				count++;
			}
		}
		
		
		
		/*temp = list.head;
		int count = 1;
		while (temp != null) {
			if (count == pos - 1) {
				Node deleted = temp.next;
				temp.next = temp.next.next;
				System.out.println("Node deleted at " + pos + " is: " + deleted.data);
				break;
			} else {
				temp = temp.next;
				count++;
			}
		}*/
		if (curNode == null) {
			System.out.println("Error: pos index exceeds max length");
			throw new LinkedListBoundException("Given position exceeds max length");
		}

		return list;
	}

	public static LinkedList deleteLastNode(LinkedList list) {

		Node curNode = head;
		Node prevNode = null;
		while (curNode.next != null) {
			prevNode = curNode;
			curNode = curNode.next;
		}
		prevNode.next = null;
		return list;
	}

	public static void display(LinkedList list) {
		Node temp = list.head;
		System.out.println("Linked list elements: ");
		while (temp != null) {
			System.out.print(temp.data + ", ");
			temp = temp.next;
		}
	}
}

public class LinkedListImpl {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListImpl.createLinkList();
	}
	
	public static void createLinkList(){
		LinkedList list = new LinkedList();
		int data = 0;
		while (data++ != 5) {
			list = LinkedList.addNode(list, new Node(data));
		}

		LinkedList.display(list);

		System.out.println("\n\nDeleting head");
		list = LinkedList.deleteHead(list);
		LinkedList.display(list);

		System.out.println("\n\nDelete from position:");
		LinkedList.deleteAtPosition(list, 5);
		LinkedList.display(list);

		System.out.println("\n\nDelete last node");
		LinkedList.deleteLastNode(list);
		LinkedList.display(list);
	}

}
