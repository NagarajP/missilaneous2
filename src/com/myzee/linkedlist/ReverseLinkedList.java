package com.myzee.linkedlist;

import com.myzee.linkedlist.MyLinkedListReverse.Node;

class MyLinkedListReverse{
	
	Node head = null;
	
	static class Node {
		int data;
		Node next;
		
		public Node(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	public void insert(Node node) {
		if(head == null) {
			System.out.println("insert head");
			head = node;
		} else {
			System.out.println("insert next");
			Node cur = head;
			while(cur.next != null) {
				cur = cur.next;
			}
			cur.next = node;
		}
	}
	
	public void display() {
		Node cur = head;
		System.out.println("\ndisplay list: ");
		while(cur != null) {
			System.out.print(cur.data + ", ");
			cur = cur.next;
		}
	}
	
	public void reverseList() {
		Node prev = null; 
        Node current = head; 
        Node next = null; 
        while (current != null) { 
            next = current.next; 
            current.next = prev; 
            prev = current; 
            current = next; 
        } 
        head = prev; 
//        return node;
		
	}
}

public class ReverseLinkedList {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyLinkedListReverse list = new MyLinkedListReverse();
		list.head = new MyLinkedListReverse.Node(85); 
        list.head.next = new Node(15); 
        list.head.next.next = new Node(4); 
        list.head.next.next.next = new Node(20); 
		list.display();
		
		System.out.println("\nreverse");
		list.reverseList();
		list.display();
	}
}
