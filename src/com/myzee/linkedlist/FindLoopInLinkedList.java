package com.myzee.linkedlist;

import java.util.HashSet;

class LinkedList1 {

	static Node head = null;

	public static LinkedList1 addNode(LinkedList1 list, Node node) {
		if (list.head == null) {
			list.head = node;
		} else {
			Node temp = list.head;
			while (temp.next != null) {
				temp = temp.next;
			}
			temp.next = node;
		}
		return list;
	}

	public static void display(LinkedList1 list) {
		Node temp = list.head;
		System.out.println("Linked list elements: ");
		while (temp != null) {
			System.out.print(temp.data + ", ");
			temp = temp.next;
		}
	}
	public static boolean detectLoop(LinkedList1 list) {
		HashSet<Node> hs = new HashSet<>();
		Node curNode = list.head;
		while(curNode != null) {
			if(hs.contains(curNode)) {
				return true;
			}
			hs.add(curNode);
			curNode = curNode.next;
		}
		return false;
	}

}

public class FindLoopInLinkedList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList1 list = new LinkedList1();
		list.addNode(list, new Node(1));
		list.addNode(list, new Node(2));
		list.addNode(list, new Node(3));
		list.addNode(list, new Node(4));
		
		list.head.next.next.next.next = list.head;
		if(LinkedList1.detectLoop(list)) {
			System.out.println("loop found");
		} else {
			System.out.println("loop does not found");
		}
		// goes to infinite loop
		LinkedList1.display(list);
	}

}