package com.myzee.linkedlist;

public class MidEleInLinkedList {

	static int flag = 0;
	static NewNode result;
	public static void findMidElement(MyLinkedList list) {
		NewNode slow = list.head;
		NewNode fast = list.head;
		
		while(fast != null && fast.next != null ) {
			slow = slow.next;
			fast = fast.next.next;
		}
		System.out.println("\nmid element is - " + slow.data);
		
	}
	/**
	 * 
	 * @param list
	 * Desc : Initialize mid element as head and initialize a counter as 0. 
	 * Traverse the list from head, while traversing increment the counter and change mid to mid->next 
	 * whenever the counter is even. So the mid will move only half of the total length of the list.
	 */
	public static void findMidElement1(MyLinkedList list) {
		// TODO Auto-generated method stub
		NewNode mid = list.head;
		NewNode cur = list.head;
		int count = 0;
		while (cur.next != null) {
			cur = cur.next;
			if(count % 2 == 0) {
				mid = mid.next;
			}
			count++;
		}
		System.out.println("mid element is ---- " + mid.data);
	}
	
	public static NewNode findMidElementRecursive(NewNode slow, NewNode fast) {
		if(fast != null && fast.next != null) {
			NewNode temp = findMidElementRecursive(slow.next, fast.next.next);
		}
		if(flag == 0) {
			result = slow;
			flag = 1;
		}
		return result;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyLinkedList list = new MyLinkedList();
		int count = 1;
		while(count <= 5) {
			NewNode node = new NewNode(count++);
			list.insert(node);
		}
		list.display();
		System.out.println();
		
		/*
		 * Method1:
		 * Using slow and fast moving poiters.
		 * slow moving poiter will move one node at a time where as fast moving pointer will move two nodes at a time.
		 * So by the time fast moving pointer reaches last node, slow moving pointer reached the middle element.
		 */
		findMidElement(list);
		
		/*
		 * Method2:
		 * Initialize mid element as head and initialize a counter as 0. Traverse the list from head, 
		 * while traversing increment the counter and change mid to mid->next 
		 * whenever the counter is odd. So the mid will move only half of the total length of the list.
		 */
		findMidElement1(list);
		
		/*
		 * Recursive way of finding mid element
		 *
		 */
		
		NewNode midNode = findMidElementRecursive(list.head, list.head );
		System.out.println("mid element is(Recursive way) - " + midNode.data);
	}

}
