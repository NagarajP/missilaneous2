package com.myzee.ropeandcoseproblemamazon;

import java.util.Arrays;

/*
 * Given lengths(costs) of ropes, find the total minimum cost of creating a single rope. 
 * (cost of two ropes = length1 + length2)
 * 
 * Explanation:
 * For example if we are given 4 ropes of lengths 4, 3, 2 and 6. We can connect the ropes in following ways.
 * 1) First connect ropes of lengths 2 and 3. Now we have three ropes of lengths 4, 6 and 5.
 * 2) Now connect ropes of lengths 4 and 5. Now we have two ropes of lengths 6 and 9.
 * 3) Finally connect the two ropes and all ropes have connected.
 * 
 * Total cost for connecting all ropes is 5 + 9 + 15 = 29. 
 * This is the optimized cost for connecting ropes. Other ways of connecting ropes would always have same or 
 * more cost. For example, if we connect 4 and 6 first (we get three strings of 3, 2 and 10), 
 * then connect 10 and 3 (we get two strings of 13 and 2). Finally we connect 13 and 2. 
 * Total cost in this way is 10 + 13 + 15 = 38.
 */
public class RopeAndCostProblem {

	public static void main(String[] args) {
//		int[] ropes = new int[]{4,2,7,6,9};
		int[] ropes = new int[]{4,2,7,6,9};
		
		findMinimumCost(ropes);
		
		for (int i = 0; i < ropes.length-1; i++) {
			Arrays.sort(ropes);
			ropes = findMinimumCost1(ropes, i);
		}
		System.out.println("Min cost is ==== " + ropes[ropes.length-1]);
	}

	private static void findMinimumCost(int[] ropes) {
		Arrays.sort(ropes);
		int prevCost = ropes[0] + ropes[1];
		int cost = prevCost;
		for(int i = 2; i < ropes.length; i++) {
			cost = cost + (prevCost + ropes[i]);
			prevCost = prevCost + ropes[i];
		}
		System.out.println("= " + cost);
	}
	
	private static int[] findMinimumCost1(int[] ropes, int i) {
		
/*
 * Solution : 
end->		0	0	0	0	28
					
					^
					|sort and sum first two non zero elements
					
			0	0	0	12	16
					
					^
					|sort and sum first two non zero elements
			
			0	0	0	16	12
			
					^
					|sort and sum first two non zero elements
			
			0	0	7	9	12
					
					^
					|sort and sum first two non zero elements
			
			0 	0 	12 	7 	9
					
					^
					|sort and sum first two non zero elements
			
			0 	6	6 	7 	9
					
					^
					|sort and sum first two non zero elements
			
			2 	4 	6 	7 	9
					
					^
					|sort
start->		
			4 	2 	7 	6 	9
		
*/
		
		int prevCost = ropes[i] + ropes[i+1];
		ropes[i] = 0;
		ropes[++i] = prevCost;
		return ropes;
	}

}
