package com.myzee.exception;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomException {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Service.analize(new ArrayList<String>(Collections.nCopies(100, "pattar")));
		} catch(Exception e) {
			System.out.println(e);
		}
	}

}

class Service {
	public static void analize(List<String> data) {
		if (data.size() > 60) {
			throw new DataLargeException("size exceeds the min len of 60");
		} else {
			System.out.println("good to go with size");
		}
	}
}

class DataLargeException extends RuntimeException {
	public DataLargeException(String exc) {
		// TODO Auto-generated constructor stub
		super(exc);
	}
}
