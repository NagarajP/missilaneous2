package com.myzee.matrix;

public class ReverseTwoDMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Matrix.createMatrix();
		 Matrix.showMatrix();
		 System.out.println("------------------------------");
		 Matrix.reverseMatrixDiagonally();
		 Matrix.showMatrix();

	}
}

class Matrix {
	
	static int[][] arr = new int[3][4];
	static int m = 3;
	static int n = 4;
	public static void createMatrix() {
		int k = 1;
		for(int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				Matrix.arr[i][j] = k++;
			}
		}
	}
	
	public static void showMatrix() {
		System.out.println("\n\nshow matrix\n");
		for(int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(arr[i][j] + "\t");
			}
			System.out.println();
		}
	}
	
	public static void reverseMatrixDiagonally() {
		for(int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if(j < n-1-j) {
					int temp = arr[i][j];
					arr[i][j] = arr[m-1-i][n-1-j];
					arr[m-1-i][n-1-j] = temp;
				}
			}
		}
	}
	
}
