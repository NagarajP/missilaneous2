package com.myzee.test;

import java.util.Calendar;
import java.util.Date;

public class AddDaysToDate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Date today = new Date();
		System.out.println(today);      //Sat Jul 14 22:25:03 IST 2018
		 
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		 
		// manipulate date
//		cal.add(Calendar.YEAR, 1);
//		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DATE, 60); 
//		cal.add(Calendar.DAY_OF_MONTH, 60);
		 
		// convert calendar to date
		Date modifiedDate = cal.getTime();
		System.out.println(modifiedDate);    
	}

}
