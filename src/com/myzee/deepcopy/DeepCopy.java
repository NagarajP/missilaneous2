package com.myzee.deepcopy;

public class DeepCopy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Emp e1 = new Emp(1, "ketherice", new Address(11, "Wuhan"));
		Emp e2 = null;
		try {
		e2 = (Emp)e1.clone();
		} catch(CloneNotSupportedException e) {
			System.out.println(e);
		}
		
		e2.getAddr().setCity("Hube");
		System.out.println(e1.getAddr().getCity());
		System.out.println(e2.getAddr().getCity());
		
	}

}

class Emp implements Cloneable{
	private int id;
	private String name;
	private Address addr;
	
	public Emp(int id, String name, Address a) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.name = name;
		this.addr = a;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddr() {
		return addr;
	}

	public void setAddr(Address addr) {
		this.addr = addr;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
//		return super.clone();
		Emp e = (Emp)super.clone();
		
		int aid = e.getAddr().getAid();
		String city = e.getAddr().getCity();
		e.setAddr(new Address(aid, city));
		
		// OR
		
		Address clonedAddr = (Address)e.getAddr().clone();
		e.setAddr(clonedAddr);
		return e;
		
	}
	
}

class Address implements Cloneable{
	private int aid;
	private String city;
	
	public Address(int aid, String city) {
		// TODO Auto-generated constructor stub
		this.aid = aid;
		this.city = city;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
}
